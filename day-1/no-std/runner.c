#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

typedef void (*solve_func)(int, char **);

int main(int argc, char **argv) {
    if (argc < 2) {
        puts("Please specify the path to the solution you want to load.");
        exit(1);
    }

    void *handle = NULL;
    solve_func solve = NULL;
    char lib[32];
    strcpy(lib, "./target/debug/");
    strncpy(lib + strlen("./target/debug/"), argv[1], sizeof(lib) - strlen("./target/debug/"));

    handle = dlopen(lib, RTLD_NOW | RTLD_GLOBAL);
    if (handle == NULL) {
        fprintf(stderr, "Unable to open lib: %s\n", dlerror());
        return -1;
    }

    solve = dlsym(handle, "main");
    if (solve == NULL) {
        fprintf(stderr, "Unable to get symbol\n");
        return -1;
    }
    solve(0, NULL);

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wformat-insufficient-args"
    printf("%lu\n");
#pragma clang diagnostic pop
}
