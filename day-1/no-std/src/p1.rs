#![feature(start)]
#![no_std]
#![forbid(unsafe_code)]

use core::panic::PanicInfo;
use core::str::FromStr;

const INPUT: &'static str = include_str!("../input");

extern fn reg_fill(_: usize, _: usize, _: usize, _: usize, _: usize, _: usize, _: usize) {}

extern fn solve() {
    let mut prev = 0;
    let mut count = 0;

    for curr in INPUT.lines().map(|line| u64::from_str(line).unwrap()) {
        if prev != 0 && prev < curr {
            count += 1;
        }
        prev = curr;
    }

    reg_fill(count, count, count, count, count, count, count)
}

#[start]
fn main(_: isize, _: *const *const u8) -> isize {
    solve();
    0
}

#[panic_handler]
fn aaaaaaaaaa(_: &PanicInfo) -> ! {
    loop {}
}
