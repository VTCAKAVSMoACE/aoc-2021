use itertools::Itertools;
use shmalloc::*;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::str::FromStr;

#[global_allocator]
static ALLOCATOR: Shmeap<
    0,
    { 1 << 24 },
    { PROT_READ | PROT_WRITE },
    { MAP_ANONYMOUS | MAP_SHARED },
    "",
> = Shmeap::new();

fn main() {
    println!(
        "{}",
        BufReader::new(File::open("input").unwrap())
            .lines()
            .map(|line| u64::from_str(&line.unwrap()).unwrap())
            .tuple_windows()
            .filter(|(prev, next)| prev < next)
            .count()
    );
}
