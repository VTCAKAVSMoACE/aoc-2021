#![feature(bool_to_option)]
#![feature(vec_retain_mut)]

use std::fmt::{Debug, Formatter, Write};
use std::str::FromStr;

struct Bingo([[Option<u8>; 5]; 5]);

impl Bingo {
    fn is_solved(&self) -> bool {
        if self.0.iter().any(|row| row.iter().all(Option::is_none)) {
            return true;
        }
        for i in 0..5 {
            if self.0.iter().flatten().skip(i).step_by(5).all(Option::is_none) {
                return true;
            }
        }
        false
    }
}

impl Debug for Bingo {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for row in self.0 {
            for item in row {
                if let Some(num) = item {
                    f.write_fmt(format_args!("{:03}", num))?;
                } else {
                    f.write_str("XXX")?;
                }
            }
            f.write_char('\n')?;
        }
        Ok(())
    }
}

fn main() {
    let file = std::fs::read_to_string("input").unwrap();
    let mut lines = file.lines();

    let nums = lines.next().unwrap().split(',').flat_map(u8::from_str).collect::<Vec<_>>();
    let mut boards = Vec::new();
    while lines.next().is_some() {
        let mut board = Bingo(Default::default());
        for row in board.0.iter_mut() {
            let mut line = lines.next().unwrap();
            for (i, mem) in [2, 3, 3, 3, 3].into_iter().zip(row.iter_mut()) {
                let (num, remaining) = line.split_at(i);
                println!("{}", num);
                let num = num.trim().parse().unwrap();
                *mem = Some(num);
                line = remaining;
            }
        }
        boards.push(board);
    }

    let mut nums = nums.into_iter();
    while let Some(num) = nums.next() {
        boards.retain_mut(|board| {
            for row in board.0.iter_mut() {
                for item in row.iter_mut() {
                    if *item == Some(num) {
                        *item = None;
                    }
                }
            }
            !board.is_solved()
        });
        if boards.len() == 1 {
            if boards.first().unwrap().is_solved() {
                println!("{:?}", boards.first().unwrap());
                println!("{}", boards.first().unwrap().0.iter().copied().flatten().filter_map(|x| x).map(|x| x as usize).sum::<usize>() * num as usize);
                return;
            }
            break;
        }
    }

    let mut board = boards.pop().unwrap();
    for num in nums {
        for row in board.0.iter_mut() {
            for item in row.iter_mut() {
                if *item == Some(num) {
                    *item = None;
                }
            }
        }
        if board.is_solved() {
            println!("{:?}", board);
            println!("{}", board.0.iter().copied().flatten().filter_map(|x| x).map(|x| x as usize).sum::<usize>() * num as usize);
            return;
        }
    }
}
