#![feature(vec_retain_mut)]

use std::ops::BitAnd;

fn main() {
    let values = std::fs::read_to_string("input").unwrap().lines().map(|line| line.bytes().map(|c| (c - b'0') as u8).collect::<Vec<_>>()).collect::<Vec<_>>();

    let mut curr = values.clone();
    for i in 0..12 {
        let mut count = 0usize;
        for bits in curr.iter() {
            count += bits[i] as usize;
        }
        let gamma = (count / (curr.len() / 2)) as u8;
        curr.retain_mut(|bits| bits[i] == gamma);
        if curr.len() == 1 {
            break;
        }
    }
    let o2 = u64::from_str_radix(&curr.pop().unwrap().into_iter().map(|c| (c + b'0') as char).collect::<String>(), 2).unwrap();
    curr = values.clone();
    for i in 0..12 {
        let mut count = 0usize;
        for bits in curr.iter() {
            count += bits[i] as usize;
        }
        let gamma = (count / (curr.len() / 2)) as u8;
        curr.retain_mut(|bits| bits[i] != gamma);
        if curr.len() == 1 {
            break;
        }
    }
    let co2 = u64::from_str_radix(&curr.pop().unwrap().into_iter().map(|c| (c + b'0') as char).collect::<String>(), 2).unwrap();

    println!("{}", o2 * co2);
}
