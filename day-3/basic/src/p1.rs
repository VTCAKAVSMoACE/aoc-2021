use std::ops::BitAnd;

fn main() {
    let mut counts = [0usize; 12];
    let mut count = 0usize;
    for values in std::fs::read_to_string("input").unwrap().lines().map(|line| line.bytes().map(|c| (c - b'0') as usize).collect::<Vec<_>>()) {
        for (count, value) in counts.iter_mut().zip(values) {
            *count += value;
        }
        count += 1;
    }
    let gamma = u64::from_str_radix(&counts.iter().map(|amount| ((amount / (count / 2)) as u8 + b'0') as char).collect::<String>(), 2).unwrap();
    let epsilon = (!gamma).bitand((1 << 12) - 1);
    println!("{:012b}", gamma);
    println!("{:012b}", epsilon);
    println!("{}", gamma * epsilon);
}
