use std::str::FromStr;

fn main() {
    let mut x = 0;
    let mut y = 0;
    for (inst, amount) in std::fs::read_to_string("input").unwrap().lines().map(|line| {
        let mut split = line.split(" ");
        (split.next().unwrap(), i64::from_str(split.next().unwrap()).unwrap())
    }) {
        match inst {
            "forward" => x += amount,
            "down" => y += amount,
            "up" => y -= amount,
            _ => unreachable!()
        }
    }
    println!("{}", x * y);
}
