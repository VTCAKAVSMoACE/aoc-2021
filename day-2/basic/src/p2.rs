use std::str::FromStr;

fn main() {
    let mut x = 0;
    let mut y = 0;
    let mut aim = 0;
    for (inst, amount) in std::fs::read_to_string("input").unwrap().lines().map(|line| {
        let mut split = line.split(" ");
        (split.next().unwrap(), i64::from_str(split.next().unwrap()).unwrap())
    }) {
        match inst {
            "forward" => {
                x += amount;
                y += aim * amount;
            },
            "down" => aim += amount,
            "up" => aim -= amount,
            _ => unreachable!()
        }
    }
    println!("{}", x * y);
}
