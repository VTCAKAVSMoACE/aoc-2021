#![feature(start)]
#![no_std]
#![forbid(unsafe_code)]

use core::panic::PanicInfo;
use core::str::FromStr;

const INPUT: &'static str = include_str!("../input");

extern fn reg_fill(_: i64, _: i64, _: i64, _: i64, _: i64, _: i64, _: i64) {}

extern fn solve() {
    let mut x = 0;
    let mut y = 0;
    let mut aim = 0;
    for (inst, amount) in INPUT.lines().map(|line| {
        let mut split = line.split(" ");
        (split.next().unwrap(), i64::from_str(split.next().unwrap()).unwrap())
    }) {
        match inst {
            "forward" => {
                x += amount;
                y += aim * amount;
            }
            "down" => aim += amount,
            "up" => aim -= amount,
            _ => unreachable!()
        }
    }

    let res = x * y;
    reg_fill(res, res, res, res, res, res, res)
}

#[start]
fn main(_: isize, _: *const *const u8) -> isize {
    solve();
    0
}

#[panic_handler]
fn aaaaaaaaaa(_: &PanicInfo) -> ! {
    loop {}
}
